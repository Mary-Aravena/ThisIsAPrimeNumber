package cl.marav.tiaPN;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ThisNumberIsPrimeNumberTest {
	String result;
	ThisNumberIsPrimeNumber test;
	
	@Before
	public void setup(){
		 test= new ThisNumberIsPrimeNumber();
	}
	
	@Test
	public void OneIsYes() {
		result= test.ThisNumberAsk(1);
		assertEquals("Yes", result);
		//fail("Not yet implemented");
	}
	
	@Test
	public void TwoIsYes(){
		result= test.ThisNumberAsk(2);
		assertEquals("Yes", result);
	}
	
	@Test
	public void FourIsNo(){
		result = test.ThisNumberAsk(4);
		assertEquals("No", result);
	
	}

}
